package com.test.messagelist;

import android.app.Application;

import com.test.messagelist.utils.GlideUtil;
import com.test.messagelist.utils.MessageListPreferences;

public class MessagesListApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MessageListPreferences.newInstance(this.getSharedPreferences("Prefs", MODE_PRIVATE));
        GlideUtil.newInstance(this);
    }
}
