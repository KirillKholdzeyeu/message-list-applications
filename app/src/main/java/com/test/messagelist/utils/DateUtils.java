package com.test.messagelist.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Date;
import java.util.Locale;

public class DateUtils extends Date {

    public static String getTimeInterval(String date) {
        Date currentDate = new Date();

        long minutes = (Math.abs(getUnixTime(date)
                - currentDate.getTime())) / (1000 * 60);

        long hours = minutes / 60;
        long days = hours / 24;
        long weeks = days / 7;
        long years = weeks / 52;

        if (years > 0) return String.format(Locale.US, "%d years ago", years);
        if (weeks > 0) return String.format(Locale.US, "%d weeks ago", weeks);
        if (days > 0) return String.format(Locale.US, "%d days ago", days);
        if (hours > 0) return String.format(Locale.US, "%d hour ago", hours);

        return String.format(Locale.US, "%d minutes ago", minutes);
    }

    public static long getUnixTime(String date) {
        DateTime dateTime = new DateTime(date, DateTimeZone.UTC);
        return dateTime.getMillis();
    }

}
