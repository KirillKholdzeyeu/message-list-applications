package com.test.messagelist.utils;

import android.content.SharedPreferences;

public class MessageListPreferences {

    private static MessageListPreferences instance;
    private SharedPreferences sharedPreferences;

    public static MessageListPreferences newInstance(SharedPreferences sharedPreferences) {
        if (instance == null) {
            synchronized (MessageListPreferences.class) {
                if (instance == null) {
                    instance = new MessageListPreferences(sharedPreferences);
                }
            }
        }
        return instance;
    }

    private MessageListPreferences(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

}
