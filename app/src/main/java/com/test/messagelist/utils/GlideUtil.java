package com.test.messagelist.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.lang.ref.WeakReference;

public class GlideUtil {

    private static GlideUtil instance;
    private static WeakReference<Context> weakContext;

    public static void newInstance(Context context) {
        if (instance == null) {
            synchronized (GlideUtil.class) {
                if (instance == null) {
                    instance = new GlideUtil(context);
                }
            }
        }
    }

    private GlideUtil(Context context) {
        weakContext = new WeakReference<>(context);
    }

    public static void setImageCircleCorners(ImageView imageView, String resource) {
        Glide.with(weakContext.get())
                .load(resource)
                .apply(RequestOptions.circleCropTransform())
                .into(imageView);
    }

}
