package com.test.messagelist.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Message implements Serializable {

    @SerializedName("id")
    private int id;
    @SerializedName("author")
    private Author author;
    @SerializedName("updated")
    private String updated;
    @SerializedName("content")
    private String content;

    public Message() {

    }

    public Message(int id, Author author, String updated, String content) {
        this.id = id;
        this.author = author;
        this.updated = updated;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
