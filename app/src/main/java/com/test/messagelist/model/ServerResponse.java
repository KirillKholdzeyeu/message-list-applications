package com.test.messagelist.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ServerResponse implements Serializable {

    @SerializedName("count")
    private int count;
    @SerializedName("messages")
    private List<Message> messages;
    @SerializedName("pageToken")
    private String pageToken;

    public ServerResponse() {

    }

    public ServerResponse(int count, List<Message> messages, String pageToken) {
        this.count = count;
        this.messages = messages;
        this.pageToken = pageToken;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public String getPageToken() {
        return pageToken;
    }

    public void setPageToken(String pageToken) {
        this.pageToken = pageToken;
    }
}
