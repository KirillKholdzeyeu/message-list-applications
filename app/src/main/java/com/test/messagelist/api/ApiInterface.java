package com.test.messagelist.api;

import com.test.messagelist.model.ServerResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("/messages")
    Observable<ServerResponse> getMessages(@Query("pageToken") String pageToken);

}
