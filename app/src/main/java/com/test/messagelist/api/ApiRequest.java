package com.test.messagelist.api;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiRequest {
    public static final String MAIN_URL = "http://message-list.appspot.com/";
    private final String TAG = ApiRequest.class.getSimpleName();
    private static ApiRequest apiRequest;
    private ApiInterface apiInterface;

    public static ApiRequest getInstance() {
        if (apiRequest == null) {
            synchronized (ApiRequest.class) {
                if (apiRequest == null) {
                    apiRequest = new ApiRequest();
                }
            }
        }
        return apiRequest;
    }

    private ApiRequest() {
        Retrofit retrofit = getRetrofit();
        apiInterface = retrofit.create(ApiInterface.class);
    }

    private Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getGsonParser()))
                .baseUrl(MAIN_URL)
                .client(getOkHttpClientBuilder().build())
                .build();
    }

    private OkHttpClient.Builder getOkHttpClientBuilder() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder().addInterceptor(interceptor);
        client.readTimeout(5 * 60, TimeUnit.SECONDS);
        client.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder requestBuilder = original.newBuilder();
            requestBuilder.header("Content-Type", "application/json");
            Request request = requestBuilder.method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        });
        return client;
    }

    private Gson getGsonParser() {
        return new GsonBuilder().addSerializationExclusionStrategy(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return f.getAnnotation(SerializedName.class) == null;
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return false;
            }
        }).create();
    }

    public ApiInterface getApi() {
        return apiInterface;
    }

}
