package com.test.messagelist.ui.base;

import android.content.Context;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseFragmentPresenter {

    private CompositeDisposable compositeDisposable;

    public void addSubscription(Disposable subscription) {
        if (compositeDisposable == null) onStart();
        compositeDisposable.add(subscription);
    }

    public void onStart() {
        compositeDisposable = new CompositeDisposable();
    }

    public void onStop() {
        if (compositeDisposable == null) return;

        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
            compositeDisposable.clear();
            compositeDisposable = null;
        }
    }

    public abstract BaseFragmentView getView();

    public Context getContext() {
        return getView().getContext();
    }

}
