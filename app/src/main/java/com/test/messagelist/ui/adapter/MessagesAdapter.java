package com.test.messagelist.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.test.messagelist.R;
import com.test.messagelist.api.ApiRequest;
import com.test.messagelist.model.Message;
import com.test.messagelist.utils.DateUtils;
import com.test.messagelist.utils.GlideUtil;
import com.test.messagelist.utils.SwipeAndDragUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements SwipeAndDragUtil.ActionCompletionContract {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private final int VIEW_TYPE_HEADER = 2;

    private List<Message> messages;
    private MessagesAdapterListener listener;
    private ItemTouchHelper itemTouchHelper;

    public MessagesAdapter(MessagesAdapterListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                view = inflater.inflate(R.layout.item_message, parent, false);
                return new MessagesHolder(view);
            case VIEW_TYPE_LOADING:
                view = inflater.inflate(R.layout.item_loading, parent, false);
                return new LoadingHolder(view);
            case VIEW_TYPE_HEADER:
                view = inflater.inflate(R.layout.item_header, parent, false);
                return new HeaderHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MessagesHolder) {
            MessagesHolder messagesHolder = (MessagesHolder) holder;
            Message message = messages.get(position);
            messagesHolder.bindViews(message);
            messagesHolder.messageContainer.setOnClickListener(v -> {
                if (listener != null) {
                    listener.messageSelected(message);
                }
            });
            messagesHolder.authorCoverIv.setOnTouchListener((v, event) -> {
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    itemTouchHelper.startDrag(holder);
                }
                return false;
            });

            if (messages != null && getItemCount() != 0 && position == messages.size() - 3) {
                listener.updateRecycler();
            }
        }
    }

    @Override
    public int getItemCount() {
        return messages == null ? 0 : messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (messages.get(position) == null) {
            return VIEW_TYPE_LOADING;
        } else if (messages.get(position) != null && messages.get(position).getAuthor() == null) {
            return VIEW_TYPE_HEADER;
        } else if (messages.get(position) != null) {
            return VIEW_TYPE_ITEM;
        }

        return 1;
    }

    @Override
    public void onViewMoved(int oldPosition, int newPosition) {
        if (messages == null || messages.size() == 0) return;

        Message targetMessage = messages.get(oldPosition);
        Message message = new Message(targetMessage.getId(), targetMessage.getAuthor(),
                targetMessage.getUpdated(), targetMessage.getContent());
        messages.remove(oldPosition);
        messages.add(newPosition, message);
        notifyItemMoved(oldPosition, newPosition);
    }

    @Override
    public void onViewSwiped(int position) {
        if (position == 0) return;
        try {
            messages.remove(position);
            notifyItemRemoved(position);
        } catch (IndexOutOfBoundsException e) {

        }
    }

    public class MessagesHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.message_container_ll)
        LinearLayout messageContainer;
        @BindView(R.id.author_name_tv)
        TextView authorNameTv;
        @BindView(R.id.author_cover_iv)
        ImageView authorCoverIv;
        @BindView(R.id.content_tv)
        TextView contentTv;
        @BindView(R.id.updated_tv)
        TextView updatedTv;

        public MessagesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindViews(Message message) {
            authorNameTv.setText(message.getAuthor().getName());
            contentTv.setText(message.getContent());
            updatedTv.setText(DateUtils.getTimeInterval(message.getUpdated()));
            GlideUtil.setImageCircleCorners(authorCoverIv,
                    ApiRequest.MAIN_URL + message.getAuthor().getPhotoUrl());
        }
    }

    public void setItemTouchHelper(ItemTouchHelper itemTouchHelper) {
        this.itemTouchHelper = itemTouchHelper;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public interface MessagesAdapterListener {
        void messageSelected(Message message);

        void updateRecycler();
    }

}
