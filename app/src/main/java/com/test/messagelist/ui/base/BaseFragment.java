package com.test.messagelist.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import java.lang.ref.WeakReference;

import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment implements BaseFragmentView {

    protected Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createPresenter();
        if (getPresenter() != null) getPresenter().onStart();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (unbinder != null) unbinder.unbind();
        if (getPresenter() != null) getPresenter().onStop();
        super.onDestroy();
    }

    public abstract BaseFragmentPresenter getPresenter();

    protected abstract void createPresenter();

    @Override
    public Context getContext() {
        return new WeakReference<>(getActivity()).get();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToastMessage(int resourceId) {
        Toast.makeText(getContext(), getContext().getString(resourceId), Toast.LENGTH_SHORT).show();
    }
}
