package com.test.messagelist.ui.base;

import android.app.Activity;
import android.content.Context;

public interface BaseContextView {

    Activity getActivity();

    Context getContext();

    void showToastMessage(String message);

    void showToastMessage(int message);

}
