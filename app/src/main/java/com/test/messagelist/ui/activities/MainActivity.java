package com.test.messagelist.ui.activities;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.test.messagelist.R;
import com.test.messagelist.ui.base.BaseActivity;
import com.test.messagelist.ui.base.BaseActivityPresenter;
import com.test.messagelist.ui.base.BaseContextView;
import com.test.messagelist.ui.fragments.MessagesFragment;
import com.test.messagelist.utils.FragmentUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements BaseContextView, NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        createToolbar(toolbar, drawerLayout);
        navigationView.setNavigationItemSelectedListener(this);
        setTitle(R.string.messages);

        FragmentUtil.replaceFragment(this, R.id.main_fragment_container, new MessagesFragment(),
                "MessagesFragment", false);
    }

    @Override
    protected BaseActivityPresenter getPresenter() {
        return null;
    }

    @Override
    protected void createPresenter() {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.messages:
                FragmentUtil.replaceFragment(this, R.id.main_fragment_container, new MessagesFragment(),
                        "MessagesFragment", false);
                break;
        }
        FragmentUtil.closeFragment(this, R.id.main_fragment_container);
        drawerLayout.closeDrawers();

        return false;
    }
}
