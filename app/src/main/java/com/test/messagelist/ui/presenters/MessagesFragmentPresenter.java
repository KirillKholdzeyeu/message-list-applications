package com.test.messagelist.ui.presenters;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.test.messagelist.R;
import com.test.messagelist.api.ApiRequest;
import com.test.messagelist.model.Message;
import com.test.messagelist.model.ServerResponse;
import com.test.messagelist.ui.adapter.MessagesAdapter;
import com.test.messagelist.ui.base.BaseFragmentPresenter;
import com.test.messagelist.ui.base.BaseFragmentView;
import com.test.messagelist.ui.fragments.MessageDetailsFragment;
import com.test.messagelist.ui.views.MessagesFragmentView;
import com.test.messagelist.utils.FragmentUtil;
import com.test.messagelist.utils.NetworkUtil;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MessagesFragmentPresenter extends BaseFragmentPresenter implements MessagesAdapter
        .MessagesAdapterListener {

    private MessagesFragmentView view;
    private ServerResponse serverResponses;
    private String pageToken;

    public MessagesFragmentPresenter(MessagesFragmentView view) {
        this.view = view;
    }

    public void updateUi(ServerResponse serverResponses) {
        if (serverResponses != null) {
            pageToken = serverResponses.getPageToken();
            view.updateRecyclerView(serverResponses.getMessages());
        }
    }

    public void getMessagesFromServer() {
        if (NetworkUtil.isNetworkConnected(getContext())) {
            Observable<ServerResponse> observable = ApiRequest.getInstance().getApi()
                    .getMessages(pageToken);
            Disposable disposable = observable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(response -> serverResponses = response,
                            e -> Log.e("Presenter", "Error", e),
                            () -> updateUi(serverResponses));

            addSubscription(disposable);
        } else {
            view.showToastMessage(R.string.network_is_not_available);
        }
    }

    public void setPageToken(String pageToken) {
        this.pageToken = pageToken;
    }

    private void openMessage(Message message) {
        MessageDetailsFragment fragment = new MessageDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(MessageDetailsFragment.EXTRA_MESSAGE, message);
        fragment.setArguments(args);

        FragmentUtil.addFragment((FragmentActivity) getContext(), R.id.main_fragment_container, fragment,
                "MessageDetailsFragment", true);
    }

    @Override
    public BaseFragmentView getView() {
        return view;
    }

    @Override
    public void messageSelected(Message message) {
        openMessage(message);
    }

    @Override
    public void updateRecycler() {
        getMessagesFromServer();
    }
}
