package com.test.messagelist.ui.views;

import com.test.messagelist.model.Message;
import com.test.messagelist.ui.base.BaseFragmentView;

import java.util.List;


public interface MessagesFragmentView extends BaseFragmentView {

    void updateRecyclerView(List<Message> messages);

}
