package com.test.messagelist.ui.base;

import android.content.Context;
import android.support.v4.app.FragmentActivity;

public interface BaseFragmentView {

    FragmentActivity getActivity();

    Context getContext();

    void showToastMessage(String message);

    void showToastMessage(int resourceId);

}
