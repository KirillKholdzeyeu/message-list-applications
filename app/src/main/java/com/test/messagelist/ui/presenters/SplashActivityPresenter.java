package com.test.messagelist.ui.presenters;

import android.content.Intent;

import com.test.messagelist.ui.activities.MainActivity;
import com.test.messagelist.ui.base.BaseActivityPresenter;
import com.test.messagelist.ui.base.BaseContextView;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SplashActivityPresenter extends BaseActivityPresenter {

    private BaseContextView view;

    public SplashActivityPresenter(BaseContextView view) {
        this.view = view;
    }

    public void closeSplash() {
        Observable<String> observable = Observable.just("").delay(1000, TimeUnit.MILLISECONDS);
        Disposable disposable = observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                }, e -> {
                }, () -> {
                    Intent intent = new Intent(view.getActivity(), MainActivity.class);
                    view.getActivity().startActivity(intent);
                    view.getActivity().finish();
                });

        addSubscription(disposable);
    }

    @Override
    public BaseContextView getView() {
        return view;
    }

}
