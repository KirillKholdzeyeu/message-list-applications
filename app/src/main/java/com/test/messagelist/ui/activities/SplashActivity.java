package com.test.messagelist.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.test.messagelist.ui.base.BaseActivity;
import com.test.messagelist.ui.base.BaseActivityPresenter;
import com.test.messagelist.ui.presenters.SplashActivityPresenter;

public class SplashActivity extends BaseActivity {

    private SplashActivityPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.closeSplash();
    }

    @Override
    protected BaseActivityPresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void createPresenter() {
        presenter = new SplashActivityPresenter(this);
    }
}
