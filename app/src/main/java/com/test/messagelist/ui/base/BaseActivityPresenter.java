package com.test.messagelist.ui.base;

import android.content.Context;
import android.widget.Toast;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseActivityPresenter {

    private CompositeDisposable compositeDisposable;

    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void bindObservable(Observable<?> observable, DisposableObserver sub) {
        addSubscription(observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(sub));
    }

    public void bindObservableWithoutComposite(Observable<?> observable, DisposableObserver sub) {
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(sub);
    }

    public void bindObservable(Observable<?> observable) {
        addSubscription(observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe());
    }

    public void addSubscription(Disposable subscription) {
        if (compositeDisposable == null) compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(subscription);
    }

    public void onStart() {
        compositeDisposable = new CompositeDisposable();
    }

    public void onStop() {
        if (compositeDisposable == null) {
            return;
        }
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
            compositeDisposable.clear();
            compositeDisposable = null;
        }
    }

    public abstract BaseContextView getView();

    public Context getContext() {
        return getView().getContext();
    }

}
