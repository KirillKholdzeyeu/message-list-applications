package com.test.messagelist.ui.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;
import android.view.View;

import com.test.messagelist.R;

import java.lang.ref.WeakReference;

import butterknife.Unbinder;

public abstract class BaseActivity extends AppCompatActivity implements BaseContextView,
        FragmentManager.OnBackStackChangedListener, View.OnClickListener {

    protected Unbinder unbinder;
    private Toolbar toolbar;
    private ActionBarDrawerToggle toogle;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createPresenter();
        if (getPresenter() != null) getPresenter().onStart();
    }

    @Override
    protected void onDestroy() {
        if (unbinder != null) unbinder.unbind();
        if (getPresenter() != null) getPresenter().onStop();
        super.onDestroy();
    }

    protected void createToolbar(Toolbar toolbar, DrawerLayout drawerLayout) {
        this.toolbar = toolbar;
        this.drawerLayout = drawerLayout;

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toogle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigaton_open,
                R.string.navigaton_closed);
        drawerLayout.addDrawerListener(toogle);
        toogle.syncState();

        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    protected abstract BaseActivityPresenter getPresenter();

    protected abstract void createPresenter();

    @Override
    public Activity getActivity() {
        return new WeakReference<>(this).get();
    }

    @Override
    public Context getContext() {
        return new WeakReference<>(this).get();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToastMessage(int message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackStackChanged() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            toolbar.setNavigationOnClickListener(null);
            toogle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigaton_open,
                    R.string.navigaton_closed);
            drawerLayout.addDrawerListener(toogle);
            toogle.syncState();
            toogle.setDrawerIndicatorEnabled(true);
        } else {
            toogle.setDrawerIndicatorEnabled(false);
            toolbar.setNavigationOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        setTitle(getString(R.string.messages));
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        Fragment currFragment = getSupportFragmentManager()
                .findFragmentById(R.id.main_fragment_container);
        trans.remove(currFragment);
        trans.commit();
        manager.popBackStack();
    }
}
