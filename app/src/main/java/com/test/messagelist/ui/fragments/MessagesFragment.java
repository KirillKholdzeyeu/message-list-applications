package com.test.messagelist.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.messagelist.R;
import com.test.messagelist.model.Message;
import com.test.messagelist.ui.adapter.MessagesAdapter;
import com.test.messagelist.ui.base.BaseFragment;
import com.test.messagelist.ui.base.BaseFragmentPresenter;
import com.test.messagelist.ui.presenters.MessagesFragmentPresenter;
import com.test.messagelist.ui.views.MessagesFragmentView;
import com.test.messagelist.utils.SwipeAndDragUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessagesFragment extends BaseFragment implements MessagesFragmentView {

    private MessagesFragmentPresenter presenter;
    private MessagesAdapter messagesAdapter;
    private List<Message> messages = new ArrayList<>();

    @BindView(R.id.messages_recycler_view)
    RecyclerView messagesRv;
//    @BindView(R.id.refresher_sr)
//    SwipeRefreshLayout refreshLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        unbinder = ButterKnife.bind(this, view);

        initRecyclerView();
//        initRefresher();

        presenter.getMessagesFromServer();

        return view;
    }

    @Override
    public BaseFragmentPresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void createPresenter() {
        presenter = new MessagesFragmentPresenter(this);
    }

//    private void initRefresher() {
//        refreshLayout.setOnRefreshListener(() -> {
//            messages.clear();
//            presenter.setPageToken("");
//            initRecyclerView();
//            presenter.getMessagesFromServer();
//            refreshLayout.setRefreshing(false);
//        });
//    }

    private void initRecyclerView() {
        messages.add(new Message(-1, null, "", ""));
        messagesAdapter = new MessagesAdapter(presenter);
        messagesRv.setLayoutManager(new LinearLayoutManager(getContext()));

        SwipeAndDragUtil swipeAndDragUtil = new SwipeAndDragUtil(messagesAdapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeAndDragUtil);

        messagesAdapter.setItemTouchHelper(itemTouchHelper);
        messagesRv.setAdapter(messagesAdapter);
        itemTouchHelper.attachToRecyclerView(messagesRv);
    }

    @Override
    public void updateRecyclerView(List<Message> messages) {
        if (this.messages.size() > 1) {
            messagesAdapter.notifyItemInserted(this.messages.size());
            this.messages.remove(this.messages.size() - 1);
            messagesAdapter.notifyItemRemoved(this.messages.size());
        }

        this.messages.addAll(messages);
        this.messages.add(null);
        ((MessagesAdapter) messagesRv.getAdapter()).setMessages(this.messages);
        messagesRv.getAdapter().notifyDataSetChanged();
    }

}
