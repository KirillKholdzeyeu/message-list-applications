package com.test.messagelist.ui.presenters;

import com.test.messagelist.api.ApiRequest;
import com.test.messagelist.model.Message;
import com.test.messagelist.ui.base.BaseFragmentPresenter;
import com.test.messagelist.ui.base.BaseFragmentView;
import com.test.messagelist.ui.views.MessageDetailsFragmentView;
import com.test.messagelist.utils.DateUtils;

public class MessageDetailsFragmentPresenter extends BaseFragmentPresenter {

    private MessageDetailsFragmentView view;

    public MessageDetailsFragmentPresenter(MessageDetailsFragmentView view) {
        this.view = view;
    }

    public void updateUi(Message message) {
        if (message != null) {
            view.setAuthorCover(ApiRequest.MAIN_URL + message.getAuthor().getPhotoUrl());
            view.setAuthorName(message.getAuthor().getName());
            view.setUpdated(DateUtils.getTimeInterval(message.getUpdated()));
            view.setContent(message.getContent());
        }
    }

    @Override
    public BaseFragmentView getView() {
        return null;
    }
}
