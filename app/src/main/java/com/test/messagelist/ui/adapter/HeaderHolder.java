package com.test.messagelist.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Copyright (c) 2017 Fishbowl. All rights reserved.
 */

class HeaderHolder extends RecyclerView.ViewHolder {

    public HeaderHolder(View view) {
        super(view);
    }

}
