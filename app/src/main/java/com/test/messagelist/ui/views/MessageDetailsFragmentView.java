package com.test.messagelist.ui.views;

import com.test.messagelist.ui.base.BaseFragmentView;

public interface MessageDetailsFragmentView extends BaseFragmentView {

    void setAuthorCover(String path);

    void setAuthorName(String name);

    void setContent(String content);

    void setUpdated(String updated);

}
