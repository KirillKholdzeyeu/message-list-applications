package com.test.messagelist.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Copyright (c) 2017 Fishbowl. All rights reserved.
 */

class LoadingHolder extends RecyclerView.ViewHolder {
    public LoadingHolder(View itemView) {
        super(itemView);
    }
}
