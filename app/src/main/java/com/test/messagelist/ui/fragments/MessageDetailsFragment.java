package com.test.messagelist.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.test.messagelist.R;
import com.test.messagelist.ui.base.BaseFragment;
import com.test.messagelist.ui.base.BaseFragmentPresenter;
import com.test.messagelist.model.Message;
import com.test.messagelist.ui.presenters.MessageDetailsFragmentPresenter;
import com.test.messagelist.ui.views.MessageDetailsFragmentView;
import com.test.messagelist.utils.GlideUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageDetailsFragment extends BaseFragment implements MessageDetailsFragmentView {

    public static final String EXTRA_MESSAGE = "com.test.messagelist.ui.fragments.extra_message";

    private MessageDetailsFragmentPresenter presenter;

    @BindView(R.id.details_author_cover_iv)
    ImageView authorCoverIv;
    @BindView(R.id.details_author_name_tv)
    TextView authorNameTv;
    @BindView(R.id.details_message_content_tv)
    TextView contentTv;
    @BindView(R.id.details_updated_tv)
    TextView updatedRv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_details, container, false);
        unbinder = ButterKnife.bind(this, view);

        Message message = (Message) getArguments().getSerializable(EXTRA_MESSAGE);

        getActivity().setTitle(message.getAuthor().getName());
        presenter.updateUi(message);

        return view;
    }

    @Override
    public void setAuthorCover(String path) {
        GlideUtil.setImageCircleCorners(authorCoverIv, path);
    }

    @Override
    public void setAuthorName(String name) {
        authorNameTv.setText(name);
    }

    @Override
    public void setContent(String content) {
        contentTv.setText(content);
    }

    @Override
    public void setUpdated(String updated) {
        updatedRv.setText(updated);
    }

    @Override
    public BaseFragmentPresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void createPresenter() {
        presenter = new MessageDetailsFragmentPresenter(this);
    }
}
